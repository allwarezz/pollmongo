<?php
require __DIR__ . '/includes/header.php';
require __DIR__ . '/includes/utils.php';
require __DIR__ . '/includes/costants.php';

if (isset($_SESSION['user'])) {
    header('Location: ' . BASE_URL . 'index.php');
}
?>

    <div class="mt-3"><h1>Registrati</h1></div>
    <?php
if (isset($_GET['stato'])) {
    \DataHandling\Utils\show_alert($_GET['stato'], $_GET['messages']);
}
?>
    <form method="POST" action="includes/user-router.php?action=registration" class="container">
      <div class="col">
        <label for="username" class="form-label">Username</label>
        <input type="text" name="username" id="username" class="form-control" required>
      </div>
      <div class="col">
        <label for="password" class="form-label">Password</label>
        <input type="password" name="password" id="password" class="form-control" required>
      </div>
      <div class="col">
        <label for="repassword" class="form-label">Re-Password</label>
        <input type="password" name="repassword" id="repassword" class="form-control" required>
      </div>
      <div class="mt-3">Sei già Registrato? <a href="./login.php">Accedi da qui</a></div>
      <div class="col mt-3">
        <input type="submit" class="btn btn-outline-primary" value="Registarti">
      </div>
    </form>
  </main>
</body>
</html>