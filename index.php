<?php
include_once 'includes/globals.php';
if (isset($_GET['stato'])) {
    \DataHandling\Utils\show_alert($_GET['stato'], $_GET['messages']);
}

if (isset($_SESSION['user'])) {
    $polls = \DataHandling\Poll::selectData();
} else {
    $polls = \DataHandling\Poll::selectData(['is_private' => false]);
}
if (count($polls) > 0):
?>
<table class="table mt-3">
    <thead>
<?php
echo \DataHandling\Utils\get_table_head_poll($polls[0]);
?>
</thead>
<tbody>
<?php
echo \DataHandling\Utils\get_table_body_poll($polls);
?>
</tbody>
</table>
<?php else: ?>
    <div class="alert alert-info" role="alert">Non ci sono votazioni</div>
<?php endif;?>
</main>
</body>
</html>