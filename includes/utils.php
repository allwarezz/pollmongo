<?php
namespace DataHandling\Utils;

function get_table_head_poll($assoc_array)
{
    $html = "<th>Titolo Votazione</th>";
    $html .= "<th>Visibilità</th>";
    $html .= "<th>Stato</th>";
    $html .= "<th></th>";
    return $html;

}

function get_table_body_poll($data)
{
    $html = '';
    foreach ($data as $votazione) {
        $html .= "<tr>";
        $html .= "<td>" . $votazione['text'] . "</td>";
        $vis = ($votazione['is_private']) ? 'Privata' : 'Pubblica';
        $html .= "<td>$vis</td>";
        $state = ($votazione['is_finished']) ? 'Finita' : 'In Corso';
        $html .= "<td>$state</td>";
        $html .= '<td>';
        if ($votazione['is_finished']) {
            $html .= '<a href="stats.php?id=' . $votazione['_id'] . '"><i class="bi bi-file-bar-graph" title="Vedi i Risultati" style="color: green" ></i><a>&nbsp;';
        } else {
            $html .= '<a href="./add-vote.php?id=' . $votazione['_id'] . '"><i class="bi bi-pen" title="Vota" style="color: blue"></i><a>&nbsp;';
        }
        if (isset($_SESSION['user'])) {
            if ($_SESSION['user']['is_admin']) {
                $html .= '<a href="./polls.php?poll-selected=' . $votazione['_id'] . '"><i class="bi bi-gear-fill" title="Gestisci Votazioni" style="color: purple"></i><a>&nbsp;';
            }
        }

        $html .= '</td>';
        $html .= "</tr>";
    }
    return $html;

}

function pretty_aggregation($result_aggregation)
{
    $options = $result_aggregation['options'];
    unset($result_aggregation['options']);
    $text = $result_aggregation['text'];
    unset($result_aggregation['text']);
    $pretty = [];
    $total_count = 0;
    foreach ($result_aggregation as $value) {
        $pretty[$value['_id']] = ['count' => $value['count']];
        $total_count += $value['count'];
    }

    foreach ($options as $v) {
        if (!array_key_exists($v, $pretty)) {
            $pretty[$v] = ['count' => 0];
        }
    }
    foreach ($pretty as $key => $val) {
        $percentage = (100 / $total_count) * $val['count'];

        $pretty[$key]['percentage'] = round($percentage, 2);
    }
    $results['stats'] = $pretty;
    $results['total_count'] = $total_count;
    $results['text'] = $text;
    return $results;
}
function show_alert($state, $messages)
{

    if ($state === "errore") {
        echo '<div class="alert alert-danger mt-3" role="alert">' . $messages . '</div>';
    } elseif ($state === "success") {
        echo '<div class="alert alert-success mt-3" role="alert">' . $messages . '</div>';
    }
}

trait InputSanitize
{
    public static function cleanInput($data)
    {
        $data = trim($data);
        $data = filter_var($data, FILTER_SANITIZE_ADD_SLASHES);
        $data = filter_var($data, FILTER_SANITIZE_SPECIAL_CHARS);
        return $data;
    }
}
