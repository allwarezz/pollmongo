<?php
include_once __DIR__ . '/header.php';
include_once __DIR__ . '/utils.php';
include_once __DIR__ . '/DBConnection.php';
include_once __DIR__ . '/FormHandle.php';
include_once __DIR__ . '/User.php';
include_once __DIR__ . '/Poll.php';
include_once __DIR__ . '/costants.php';

$uri_fragments = explode("/", $_SERVER['SCRIPT_FILENAME']);
if (!isset($_SESSION['user']) && $uri_fragments[count($uri_fragments) - 1] !== 'index.php'
    && $uri_fragments[count($uri_fragments) - 1] !== 'add-vote.php' && $uri_fragments[count($uri_fragments) - 1] !== 'stats.php') {
    header('Location: ' . BASE_URL . 'login.php');
}
