<?php
namespace DataHandling;

use \DataHandling\Utils\InputSanitize;

class User
{
    use \DataHandling\Utils\InputSanitize;
    public static function registerUser($form_data)
    {

        $fields = array(
            'username' => $form_data['username'],
            'password' => $form_data['password'],
            'repassword' => $form_data['repassword'],
        );

        $fields = self::sanitize($fields);

        if ($fields['password'] !== $fields['repassword']) {
            header('Location: ' . BASE_URL . 'registration.php?stato=errore&messages=Le password non corrispondono');
            exit;
        }

        $db_conn = \DBHandle\getConnection();
        $resFind = $db_conn->users->findOne([
            'username' => $fields['username'],
        ], []);
        if (!is_null($resFind)) {
            header('Location: ' . BASE_URL . 'registration.php?stato=errore&messages=Username già utilizzato');
            exit;
        }
        $resInsert = $db_conn->users->insertOne([
            'username' => $fields['username'],
            'password' => password_hash($fields['password'], PASSWORD_DEFAULT),
            'is_admin' => false,
            'polls' => [],
        ]);
        if ($resInsert->getInsertedCount() > 0) {
            header('Location: ' . BASE_URL . 'registration.php?stato=success&messages=Utente inserito con successo');
            exit;
        }

        header('Location: ' . BASE_URL . 'registration.php?stato=errore&messages='
            . 'Problema nell\'inserimento del nuovo utente');
        exit;

    }

    public static function loginUser($form_data)
    {
        $fields = array(
            'username' => $form_data['username'],
            'password' => $form_data['password'],
        );

        $fields = self::sanitize($fields);

        $db_conn = \DBHandle\getConnection();
        $resFind = $db_conn->users->findOne([
            'username' => $fields['username'],
        ], []);

        if (is_null($resFind)) {
            header('Location: ' . BASE_URL . 'login.php?stato=errore&messages=Login Fallito');
            exit;
        }

        $results = array();

        if (get_class($resFind) === 'MongoDB\Model\BSONDocument') {
            $results[] = iterator_to_array($resFind);

        }
        if (!password_verify($fields['password'], $results[0]['password'])) {
            header('Location: ' . BASE_URL . 'login.php?stato=errore&messages=Login Fallito');
            exit;
        }

        return $results;

    }

    protected static function sanitize($fields)
    {
        $fields['username'] = self::cleanInput($fields['username']);

        return $fields;
    }
}
