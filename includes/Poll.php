<?php
namespace DataHandling;

use \DataHandling\Utils\InputSanitize;

class Poll extends FormHandle
{
    use \DataHandling\Utils\InputSanitize;

    public static function insertData($form_data, $id = null)
    {
        $fields = self::sanitize($form_data);
        $options = [];
        foreach ($fields as $k => $v) {
            if ($v === '') {
                header('Location: ' . BASE_URL . 'polls.php?stato=errore'
                    . '&messages=Tutti i campi di Testo sono obbligatori!');
                exit;
            }
            if (str_starts_with($k, 'option-')) {
                $options[] = $fields[$k];
            }
        }
        if (isset($fields['is_private']) && $fields['is_private'] === 'on') {
            $fields['is_private'] = true;
        } else {
            $fields['is_private'] = false;
        }
        $db_conn = \DBHandle\getConnection();

        $resInsert = $db_conn->polls->insertOne([
            'text' => $fields['text'],
            'is_finished' => false,
            'is_private' => $fields['is_private'],
            'options' => $options,
            'votes' => [],
        ]);
        if ($resInsert->getInsertedCount() > 0) {
            header('Location: ' . BASE_URL . 'index.php?stato=success&messages=Votazione Inserita Correttamente');
            exit;
        }

        header('Location: ' . BASE_URL . 'polls.php?stato=errore&messages=Errore nell\'inserimento della votazione');
        exit;
    }

    public static function selectData($args = null, $id = null)
    {
        $db_conn = \DBHandle\getConnection();
        if (is_null($args) && is_null($id)) {
            $resFind = $db_conn->polls->find([], []);
        } elseif (isset($args['is_private']) && is_null($id)) {
            $resFind = $db_conn->polls->find($args, []);
        } elseif (is_null($args) && isset($id)) {
            try {
                $resFind = $db_conn->polls->find([
                    '_id' => new \MongoDB\BSON\ObjectID($id),
                ], []);
            } catch (\Exception $e) {
                error_log("Errore PHP in linea {$e->getLine()}: " . $e->getMessage() . "\n", 3, 'my-errors.log');
                header('Location: ' . BASE_URL . 'index.php?stato=errore'
                    . '&messages=Ops, Errore Generico Votazione');
                exit;
            }

        }

        if (is_null($resFind)) {
            return [];
        }

        $results = array();

        if (get_class($resFind) !== 'MongoDB\Model\BSONDocument') {
            foreach ($resFind as $document) {
                $temp = iterator_to_array($document);
                foreach ($temp as $k => $v) {
                    if (is_string($v)) {
                        $temp[$k] = stripslashes($v);
                    } else {
                        $temp[$k] = $v;
                    }

                }
                $results[] = $temp;

            }
        }

        return $results;
    }

    public static function updateData($form_data, $id)
    {
        $fields = null;
        if (!is_null($form_data)) {
            $fields = self::sanitize($form_data);
        }
        $db_conn = \DBHandle\getConnection();
        try {
            if (!is_null($fields) && isset($fields['vote']) && !is_null($id)) {
                $args = ['_id' => new \MongoDB\BSON\ObjectID($id), 'is_finished' => false];
                if (!isset($_SESSION['user'])) {
                    $args['is_private'] = false;
                }
                $res = $db_conn->polls->updateOne([
                    '_id' => new \MongoDB\BSON\ObjectID($id),
                ], [
                    '$push' => ['votes' => $fields['vote']],
                ]);
                if ($res->getModifiedCount() > 0) {
                    if (isset($_SESSION['user'])) {
                        $res = $db_conn->users->updateOne([
                            '_id' => new \MongoDB\BSON\ObjectID($_SESSION['user']['_id']),
                        ], [
                            '$push' => ['polls' => $id],
                        ]);
                    }

                }
            } elseif (is_null($fields) && !is_null($id)) {
                $args = ['_id' => new \MongoDB\BSON\ObjectID($id)];
                $res = $db_conn->polls->updateOne([
                    '_id' => new \MongoDB\BSON\ObjectID($id),
                ], [
                    '$set' => ['is_finished' => true],
                ]);
                if ($res->getModifiedCount() > 0) {
                    header('Location: ' . BASE_URL . 'polls.php?poll-selected=' . $id . '&stato=success'
                        . '&messages=Votazione CHIUSA!');
                    exit;
                } else {
                    header('Location: ' . BASE_URL . 'polls.php?poll-selected=' . $id . '&stato=errore'
                        . '&messages=La votazione era già chiusa!');
                    exit;
                }
            }
        } catch (\Exception $e) {
            error_log("Errore PHP in linea {$e->getLine()}: " . $e->getMessage() . "\n", 3, 'my-errors.log');
            header('Location: ' . BASE_URL . 'index.php?stato=errore'
                . '&messages=Errore non è possibile votare!');
            exit;
        }
        header('Location: ' . BASE_URL . 'index.php?stato=success'
            . '&messages=Voto Acquisito Correttamente! Grazie');
        exit;

    }

    public static function deleteData($id)
    {
        if (isset($_SESSION['user']) && $_SESSION['user']['is_admin']) {
            try {
                $db_conn = \DBHandle\getConnection();
                $pollDeleted = $db_conn->polls->deleteOne([
                    "_id" => new \MongoDB\BSON\ObjectID($id),
                ]);

                if ($pollDeleted->getDeletedCount() > 0) {
                    header('Location: ' . BASE_URL . 'index.php?stato=success'
                        . '&messages=Votazione Eliminata Correttamente');
                    exit;
                } else {
                    error_log("Errore MongoDB nella cancellazione della Votazione" . "\n", 3, 'my-errors.log');
                    header('Location: ' . BASE_URL . 'polls.php?poll-selected=' . $id . '&stato=errore'
                        . '&messages=Non è possibile Eliminare la Votazione, Riprova più Tardi');
                    exit;
                }
            } catch (\Exception $e) {
                header('Location: ' . BASE_URL . 'polls.php?poll-selected=' . $id . '&stato=errore'
                    . '&messages=Non è possibile Eliminare la Votazione, Riprova più Tardi');
                exit;
            }

        } else {
            header('Location: ' . BASE_URL . 'index.php?stato=errore'
                . '&messages=Non Autorizzato');
            exit;
        }
    }

    public static function getStats($id)
    {
        $db_conn = \DBHandle\getConnection();
        $poll_selected = self::selectData(null, $id);
        if ($poll_selected[0]['is_private'] && !isset($_SESSION['user'])) {
            header('Location: ' . BASE_URL . 'index.php?stato=errore'
                . '&messages=Non Autorizzato, Votazione Privata');
            exit;
        }
        if (!$poll_selected[0]['is_finished']) {
            header('Location: ' . BASE_URL . 'index.php?stato=errore'
                . '&messages=Votazione In Corso, Non puoi visualizare i Risultati');
            exit;
        }
        $resAgg = $db_conn->polls->aggregate(
            [
                ['$match' => ['_id' => new \MongoDB\BSON\ObjectID($id)]],
                ['$project' => ['_id' => 0, 'votes' => 1, 'options' => 1, 'text' => 1]],
                ['$unwind' => ['path' => '$votes']],
                ['$group' => ['_id' => '$votes', 'count' => ['$sum' => 1], 'options' => ['$first' => '$options'],
                    'text' => ['$first' => '$text']]],
                ['$sort' => ['_id' => 1]],
            ]
        );
        $results = [];
        $options = false;
        $text = false;
        foreach ($resAgg as $document) {
            $temp = iterator_to_array($document);
            if (!$options) {
                $options = iterator_to_array($temp['options']);
            }
            if (!$text) {
                $text = stripslashes($temp['text']);
            }
            unset($temp['options']);
            unset($temp['text']);
            $results[] = $temp;
        }
        $results['options'] = $options;
        $results['text'] = $text;
        return $results;
    }

    protected static function sanitize($fields)
    {
        foreach ($fields as $k => $v) {
            $fields[$k] = self::cleanInput($v);
        }

        return $fields;
    }
}
