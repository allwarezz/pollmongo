<?php

$uri_fragments = explode("/", $_SERVER['SCRIPT_FILENAME']);

if ((!isset($_SESSION['user']) || !$_SESSION['user']['is_admin']) && $uri_fragments[count($uri_fragments) - 1] !== 'stats.php') {
    header('Location: ' . BASE_URL . 'index.php?stato=errore&messages=Non Autorizzato');
    exit;
}
