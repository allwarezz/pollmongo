<?php
session_start();
include_once __DIR__ . '/utils.php';
include_once __DIR__ . '/DBConnection.php';
include_once __DIR__ . '/User.php';
include_once __DIR__ . '/costants.php';

if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['user']);
    header('Location: ' . BASE_URL . 'login.php');
    exit;
}

if (!isset($_GET['action'])) {
    header('Location: ' . BASE_URL . 'index.php');
}

if (isset($_GET['action']) && $_GET['action'] === 'registration') {
    \DataHandling\User::registerUser($_POST);
}

if (isset($_GET['action']) && $_GET['action'] === 'login') {
    $loggedInUser = \DataHandling\User::loginUser($_POST);
    $_SESSION['user'] = $loggedInUser[0];
    header('Location: ' . BASE_URL);
    exit;
}
