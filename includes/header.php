<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="./assets/styles/my-style.css">
    <title>Votazioni</title>
</head>
<body class="container">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="./index.php"><i class="bi bi-bar-chart-line"> Votazioni</i></a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <?php if (isset($_SESSION['user']) && $_SESSION['user']['is_admin']): ?>
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="./polls.php">Gestione Votazioni</a>
        </li>
        <?php endif;?>
      </ul>
      <form class="d-flex">
          <?php if (!isset($_SESSION['user'])): ?>
            <a class="btn btn-outline-primary" href="./registration.php">Registarti</a>&nbsp;
            <a class="btn btn-outline-primary" href="./login.php">Login</a>
      <?php
endif;
if (isset($_SESSION['user'])):

?>
	<ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" href="#">
              <?php echo ($_SESSION['user']['is_admin']) ? '<i class="bi bi-person-circle"></i>' : '<i class="bi bi-person-fill"></i>'; ?>
              <strong><?php echo $_SESSION['user']['username']; ?></strong>
            </a>
        </li>
      </ul><a class="btn btn-outline-primary" href="./includes/user-router.php?logout=1">Esci</a>
	<?php endif;?>

      </form>
    </div>
  </div>
</nav>
<main>
