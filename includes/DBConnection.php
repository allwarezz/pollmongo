<?php
namespace DBHandle;

require_once __DIR__ . '/../vendor/autoload.php';

function getConnection()
{
    try {
        $client = new \MongoDB\Client(
            'mongodb+srv://daniele:2indRa9046MPyrMM@cluster0.e6o5g.mongodb.net/poll?retryWrites=true&w=majority'
        );
        $db = $client->poll;

        $listCollections = iterator_to_array($db->listCollectionNames());
        if (!in_array('users', $listCollections)) {
            $db->createCollection("users", [
                'validator' => [
                    '$jsonSchema' => [
                        'bsonType' => "object",
                        'required' => ["username", "password", 'is_admin', 'polls'],
                        'properties' => [
                            'username' => [
                                'bsonType' => "string",
                                'description' => 'Deve essere una stringa ed è obbligatorio.',
                            ],
                            'password' => [
                                'bsonType' => 'string',
                                'description' => 'Deve essere una stringa ed è obbligatorio',
                            ],
                            'is_admin' => [
                                'bsonType' => 'bool',
                                'description' => 'Deve essere un booleano ed è obbligatoria',
                            ],
                            'polls' => [
                                'bsonType' => 'array',
                                'description' => 'array di ObjectId poll, obbligatorio',
                                'items' => [
                                    'bsonType' => 'string',
                                    'description' => 'elemento di objectId pool',
                                ],
                            ],
                        ],
                    ],
                ],
            ]);
            $db->users->insertOne([
                'username' => 'admin',
                'password' => password_hash('admin', PASSWORD_DEFAULT),
                'is_admin' => true,
                'polls' => array(),
            ]);
        }

        if (!in_array('polls', $listCollections)) {
            $db->createCollection("polls", [
                'validator' => [
                    '$jsonSchema' => [
                        'bsonType' => "object",
                        'required' => ["text", 'is_finished', 'is_private', 'options', 'votes'],
                        'properties' => [
                            'text' => [
                                'bsonType' => "string",
                                'description' => 'Deve essere una stringa ed è obbligatorio.',
                            ],
                            'is_finished' => [
                                'bsonType' => 'bool',
                                'description' => 'Deve essere un booleano ed è obbligatorio',
                            ],
                            'is_private' => [
                                'bsonType' => 'bool',
                                'description' => 'Deve essere un booleano ed è obbligatorio',
                            ],
                            'options' => [
                                'bsonType' => 'array',
                                'required' => [
                                    'items',
                                ],
                                'description' => 'array di stringhe, di opzioni',
                                'items' => [
                                    'bsonType' => 'string',
                                    'description' => 'opzione, campo obbligatorio',
                                ],
                            ],
                            'votes' => [
                                'bsonType' => 'array',
                                'description' => 'array di stringhe, di voti, non obbligatorio',
                                'items' => [
                                    'bsonType' => 'string',
                                    'description' => 'voto',
                                ],
                            ],
                        ],
                    ],
                ],
            ]);
        }
    } catch (\Exception $e) {
        echo "Errore di connessione";
    }
    return $db;
}
