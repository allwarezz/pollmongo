<?php
include_once 'includes/globals.php';
if (isset($_GET['stato'])) {
    \DataHandling\Utils\show_alert($_GET['stato'], $_GET['messages']);
}

if (isset($_GET['id'])) {
    $poll_aggregation = \DataHandling\Poll::getStats($_GET['id']);

} else {
    header('Location: ' . BASE_URL . 'index.php?stato=errore&messages=Votazione Mancante');
    exit;
}

if ($poll_aggregation):
    $results = \DataHandling\Utils\pretty_aggregation($poll_aggregation);
    $data_stats = $results['stats'];
    $total_votes = $results['total_count'];
    $title = $results['text'];

    ?>
	<div class="container body-container">

					<p class="p-2 w-100"><center><h4><?php echo $title; ?></h4><small>Voti Totali(<?php echo $total_votes; ?>)</small></center</p><br/><br/>
	<?php foreach ($data_stats as $k => $v): ?>
	    <p class="skill w-100 p-3" style="background: linear-gradient(to right, #614385 0%,#516395 <?php echo $v['percentage'] ?>%,#D3D3D3 <?php echo $v['percentage'] ?>%)">
		  <span  class="span-text" ><?php echo $k; ?></span>
		  <span class="span-text"><?php echo $v['percentage'] ?>% (<?php echo $v['count'] ?> voti)</span>
		</p>


	<?php endforeach;?>
</div>
<?php else: ?>
<div class="alert alert-info mt-3" role="alert">Non sono ancora stati espressi voti per questa votazione</div>
<?php endif;?>