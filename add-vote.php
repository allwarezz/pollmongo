<?php
include_once 'includes/globals.php';
if (isset($_GET['stato'])) {
    \DataHandling\Utils\show_alert($_GET['stato'], $_GET['messages']);
}

if (isset($_GET['id'])) {
    if (!isset($_GET['vote'])) {

        $poll_selected = \DataHandling\Poll::selectData(null, $_GET['id']);
        if (!$poll_selected) {
            header('Location: ' . BASE_URL . 'index.php?stato=errore&messages=Votazione Non Esistente');
            exit;
        }
        if ($poll_selected[0]['is_private'] && !isset($_SESSION['user'])) {
            header('Location: ' . BASE_URL . 'index.php?stato=errore&messages=Non sei autorizzato ad accedere a questa Votazione');
            exit;
        }
        if ($poll_selected[0]['is_finished']) {
            header('Location: ' . BASE_URL . 'index.php?stato=errore&messages=Votazione conclusa, non puoi votare');
            exit;
        }
    } else {
        \DataHandling\Poll::updateData(['vote' => $_GET['vote']], $_GET['id']);
    }
} else {
    header('Location: ' . BASE_URL . 'index.php?stato=errore&messages=Votazione Mancante');
    exit;
}

$poll_selected[0]['options'] = iterator_to_array($poll_selected[0]['options']);
?>
<h5 class="mt-5">Votazione Selezionata</h5>
<fieldset class="p-2 mt-3" style="border: 1px solid grey">
    <div class="row">
        <div class="col-12 mt-3">
            <label>Titolo</label>
            <input class="form-control" type="text" value="<?php echo $poll_selected[0]['text'] ?>" disabled>
        </div>

        <h6 class="mt-5">Clicca sull'opzione che vuoi votare:</h6>
    <?php $idx = 1;foreach ($poll_selected[0]['options'] as $option): ?>
        <div class="col-4 mt-3">
            <label>Opzione <?php echo $idx++; ?></label>
            <a href="./add-vote.php?vote=<?php echo $option ?>&id=<?php echo $poll_selected[0]['_id']; ?>" class=" btn btn-outline-success w-100"><?php echo $option ?></a>
        </div>
    <?php endforeach;?>
    </div>
</fieldset>
