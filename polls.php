<?php
include_once 'includes/globals.php';
include_once 'includes/acl-admin.php';

if (isset($_GET['stato'])) {
    \DataHandling\Utils\show_alert($_GET['stato'], $_GET['messages']);
}

$options = 0;
if (isset($_GET['num_options']) && intval($_GET['num_options']) > 1 && intval($_GET['num_options']) <= 6) {
    $options = intval($_GET['num_options']);
}
if (isset($_GET['num_options']) && intval($_GET['num_options']) > 6) {
    \DataHandling\Utils\show_alert('errore', 'Numero opzioni non valido, deve essere compreso tra 2 e 6');
}

if (isset($_GET['stato'])) {
    \DataHandling\Utils\show_alert('error', 'Numero opzioni non valido, deve essere compreso tra 2 e 6');
}
if (isset($_GET['poll-selected'])) {
    $poll_selected = $_GET['poll-selected'];
}
$polls = \DataHandling\Poll::selectData();
?>
<?php if ($options > 1): ?>
<h2 class="mt-3">Gestione Votazioni</h2>
<form action="includes/poll-router.php?action=add" method="post">
<fieldset class="p-2" style="border: 1px solid grey">
  <legend>Nuova Votazione</legend>
  <div class="row">
    <div class="col-12 mt-3">
        <label>Titolo</label>
        <input class="form-control" type="text" name="text" required>
    </div>
    <?php for ($i = 1; $i <= $options; $i++): ?>
        <div class="col-4 mt-3">
        <label>Opzione <?php echo $i; ?></label>
            <input class="form-control" type="text" name="option-<?php echo $i; ?>" required>
        </div>
    <?php endfor;?>
    </div>
    <div class="col-4 mt-3">
        <label>Privato </label>
            <input type="checkbox" name="is_private">
        </div>
    <div class="col-1 offset-10 mt-3">
        <input class="btn btn-primary w-100" type="submit" value="Crea">
    </div>
</div>
</fieldset>
    </form>
<?php else: ?>
<form action="" method="get">
<h3>Crea una Nuova Votazioni</h3>
<fieldset class="p-2" style="border: 1px solid grey">
  <div class="row">
      <div class="col-8">Scegli quante opzioni dovrà avere la votazione</div>
    <div class="col-2">
        <input class="form-control" type="number" value="2" min="2" max="6" name="num_options">
    </div>
    <div class="col-2">
        <input class="btn btn-primary" type="submit" value="Elabora">
    </div>
</div>
</fieldset>
</form>
<hr/>
<?php endif;?>
<?php if ($polls && $options === 0): ?>
			<br/><br/>
            <form action="" method="get">
                <h3>Gestisci Votazioni</h3>
                <fieldset class="p-2" style="border: 1px solid grey">
                    <div class="row">
                        <div class="col-12">
                            <label for="associato">Scegli:</label>
                            <select class="form-control" name="poll-selected" id="">
                                <?php foreach ($polls as $k => $v):
    if (isset($poll_selected) && !is_array($poll_selected)) {
        if ($v['_id'] == $poll_selected) {
            $poll_selected = $polls[$k];
        }
    }
    ?>
									<option value="<?php echo $v['_id']; ?>"><?php echo $v['text']; ?></option>
									<?php endforeach;?>
            </select>
        </div>
    </div>
    <div class="row">
<div class="col-2 offset-10 mt-3">
    <input type="submit" class="btn btn-primary" value="Seleziona">
</div>
    </div>
    </fieldset>
</form>
<?php endif;?>

<?php if (isset($poll_selected) && is_array($poll_selected) && $options === 0):
    $poll_selected['options'] = iterator_to_array($poll_selected['options']);
    ?>
														<h5 class="mt-5">Votazione Selezionata</h5>
													    <fieldset class="p-2 mt-3" style="border: 1px solid grey">
														<div class="row">
														 <div class="col-12 mt-3">
														   <label>Titolo</label>
								<input class="form-control" type="text" value="<?php echo $poll_selected['text'] ?>" disabled>
									 </div>
											 <?php $idx = 1;foreach ($poll_selected['options'] as $option): ?>
											<div class="col-4 mt-3">
										<label>Opzione <?php echo $idx++; ?></label>
											<input class="form-control" type="text" value="<?php echo $option ?>" disabled>
										</div>
											<?php endforeach;?>
    </div>
    <div class="col-4 mt-3">
        <label>Privato </label>
            <input type="checkbox" name="is_private" <?php echo ($poll_selected['is_private']) ? 'checked' : ''; ?> disabled>
        </div>

</div>
		    <div class="row mt-3">
				<div class="col-4 mt-3">
		            <a class="btn btn-outline-danger w-100" href="./includes/poll-router.php?action=delete&id=<?php echo $poll_selected['_id']; ?>">Elimina Votazione</a>
				</div>
		        <div class="col-4 mt-3">
					<a class="btn btn-outline-secondary w-100 <?php echo ($poll_selected['is_finished']) ? 'disabled' : ''; ?>" href="./includes/poll-router.php?action=update&id=<?php echo $poll_selected['_id']; ?>">Chiudi Votazione</a>
		        </div>
				<div class="col-4 mt-3">
					<a class="btn btn-outline-primary w-100 <?php echo ($poll_selected['is_finished']) ? '' : 'disabled'; ?>" href="stats.php?id=<?php echo $poll_selected['_id']; ?>">Vedi Risultati</a>
				</div>
		    </div>
    </fieldset>
	<?php endif;?>
<br/>
<br/>

</main>
</body>
</html>
